package com.projectwork.demo.constants;

public enum Events {
    prModify,
    frModify,
    prConfirm,
    frConfirm,
    prRefused,
    frRefused
}
