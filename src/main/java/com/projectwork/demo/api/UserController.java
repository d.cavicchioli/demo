package com.projectwork.demo.api;

import com.projectwork.demo.model.OrderBusiness;
import com.projectwork.demo.model.User;
import com.projectwork.demo.model.UserCompanyRole;
import com.projectwork.demo.service.OrderService;
import com.projectwork.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private final UserService userService;

    @GetMapping(value = "/user/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<User> getAllUsers() {
        return userService.findAllUsers();
    }

    @GetMapping(value = "/user/role/company/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<UserCompanyRole> getAllUsersCompanyRole() {
        return userService.findAllUsersCompanyRole();
    }

    @GetMapping(value = "/user/role/company/all/{userId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<UserCompanyRole> getUserCompanyRoles(@PathVariable("userId")  String userId) {
        return userService.findUserCompanyRoles(Integer.parseInt(userId));
    }


}
