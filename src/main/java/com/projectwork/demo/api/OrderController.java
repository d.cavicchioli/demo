package com.projectwork.demo.api;

import com.projectwork.demo.model.OrderBusiness;
import com.projectwork.demo.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/")
public class OrderController {

    @Autowired
    private final OrderService orderService;

    @GetMapping(value = "/order/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<OrderBusiness> getAllOrders() {
        return orderService.findAll();
    }

    @GetMapping(value = "/order/all/{companyId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<OrderBusiness> getAllOrdersByCompany(@PathVariable("companyId")  String companyId) {
        return orderService.findByCompanyId(Integer.parseInt(companyId));
    }
}
