package com.projectwork.demo.api;

import com.projectwork.demo.model.Company;
import com.projectwork.demo.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CompanyController {

    @Autowired
    private final CompanyService companyService;


    @GetMapping(value = "/company/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Company> getAllCompanies() {
        return companyService.findAllCompanies();
    }

    @GetMapping(value = "/company/{companyId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Company getCompanyById(@PathVariable("companyId") String comapanyId) {
        return companyService.findAllCompanies().get(0);
    }
}
