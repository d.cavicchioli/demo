package com.projectwork.demo.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@JsonTypeName("permission")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@Table(name = "permission")
public class Permission implements Serializable {

    @Id
    @Column(name = "PERMISSION_ID", nullable = false)
    public int permissionId;

    @Basic
    @Column(name = "PERMISSION_NAME", nullable = true, length = 100)
    public String permissionName;

//    @ManyToMany(mappedBy = "permissions")
//    Set<Role> roles;

}
