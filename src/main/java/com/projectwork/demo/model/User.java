package com.projectwork.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@JsonTypeName("user")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Column(name = "USER_ID", nullable = false)
    public int userId;

    @Basic
    @Column(name = "USERNAME", nullable = true, length = 100)
    public String username;

    @Basic
    @Column(name = "PASSWORD", nullable = true, length = 100)
    public String password;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
//    public Collection<UserCompanyRole> usersUserCompanyRoles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User company = (User) o;
        return userId == company.userId && Objects.equals(username, company.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username);
    }

}
