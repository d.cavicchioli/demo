package com.projectwork.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
//@JsonTypeName("order")
//@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@Table(name = "orderbusiness")
public class OrderBusiness implements Serializable {

    @Id
    @Column(name = "ORDER_ID", nullable = false)
    public int orderId;

    @Basic
    @Column(name = "ORDER_TYPE", nullable = false, length = 100)
    public String orderType;

    @Basic
    @Column(name = "ORDER_NAME", nullable = false, length = 100)
    public String orderName;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "WF_ID", referencedColumnName = "WORKFLOW_ID", nullable = false)
    public Workflow workflow;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "DITTA_ID", referencedColumnName = "COMPANY_ID", nullable = false)
    public Company company;

    @OneToMany(mappedBy = "orderBusiness")
    public Collection<Orderline> orderlines;

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderType, orderName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderBusiness orderBusiness = (OrderBusiness) o;
        return orderId == orderBusiness.orderId && Objects.equals(orderType, orderBusiness.orderType) && Objects.equals(orderName, orderBusiness.orderName);
    }

}
