package com.projectwork.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
//@JsonTypeName("orderline")
//@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@Table(name = "orderline")
public class Orderline implements Serializable {

    @Id
    @Column(name = "ORDER_LINE_ID", nullable = false)
    public int orderLineId;

    @Basic
    @Column(name = "STATE", nullable = true, length = 100)
    public String state;

    @Basic
    @Column(name = "QUANTITY", nullable = true, precision = 0)
    public Double quantity;

    @Basic
    @Column(name = "PRICE", nullable = true, precision = 0)
    public Double price;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ORDER_ID", nullable = false)
    public OrderBusiness orderBusiness;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orderline orderline = (Orderline) o;
        return orderLineId == orderline.orderLineId && Objects.equals(state, orderline.state) && Objects.equals(quantity, orderline.quantity) && Objects.equals(price, orderline.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderLineId, state, quantity, price);
    }
}
