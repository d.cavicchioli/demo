package com.projectwork.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
@JsonTypeName("transaction")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@Table(name = "transaction")
public class Transaction implements Serializable {

    @Id
    @Column(name = "TRANSACTION_ID", nullable = false)
    public int transactionId;

    @Basic
    @Column(name = "INITIAL_STATE", nullable = true, length = 100)
    public String initialState;

    @Basic
    @Column(name = "FINAL_STATE", nullable = true, length = 100)
    public String finalState;

    @Basic
    @Column(name = "EVENT", nullable = true, length = 100)
    public String event;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne
    @JoinColumn(name = "WF_ID", referencedColumnName = "WORKFLOW_ID", nullable = false)
    public Workflow workflow;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return transactionId == that.transactionId && Objects.equals(initialState, that.initialState) && Objects.equals(finalState, that.finalState) && Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, initialState, finalState, event);
    }
}
