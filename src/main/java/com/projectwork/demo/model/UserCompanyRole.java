package com.projectwork.demo.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "usercompanyrole")
public class UserCompanyRole implements Serializable {

    @Id
    @Column(name = "USERCOMPANYROLE_ID", nullable = false)
    public int usercompanyroleId;

//    @Basic
//    @Column(name = "USER_ID", nullable = false)
//    public String userId;
//
//    @Basic
//    @Column(name = "COMPANY_ID", nullable = false)
//    public String companyId;
//
//    @Basic
//    @Column(name = "ROLE_ID", nullable = false)
//    public String roleId;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", referencedColumnName = "COMPANY_ID", nullable = false)
    public Company company;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    public User user;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID", nullable = false)
    public Role role;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usercompanyrole")
//    public Collection<User> users;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usercompanyrole")
//    public Collection<Company> companies;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usercompanyrole")
//    public Collection<Role> roles;

}
